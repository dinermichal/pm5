package cz.altairsoftware.pm5.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import cz.altairsoftware.pm5.ui.fragments.WelcomeImageViewPagerFragment;

import static cz.altairsoftware.pm5.utils.Constants.FIRST_PAGE;
import static cz.altairsoftware.pm5.utils.Constants.SECOND_PAGE;
import static cz.altairsoftware.pm5.utils.Constants.THIRD_PAGE;

public class WelcomeImageAdapter extends FragmentPagerAdapter {

    public WelcomeImageAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int page) {

        switch (page) {
            case FIRST_PAGE:
                return WelcomeImageViewPagerFragment.init(page);
            case SECOND_PAGE:
                return WelcomeImageViewPagerFragment.init(page);
            case THIRD_PAGE:
                return WelcomeImageViewPagerFragment.init(page);
        }
        return null;
    }
}