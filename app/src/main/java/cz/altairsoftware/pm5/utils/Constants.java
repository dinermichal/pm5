package cz.altairsoftware.pm5.utils;

public class Constants {
    public static final int SIGN_IN = 1;
    public static final int SIGN_UP = 1;
    public static final String WHERE_TO = "WHERE_TO";

    public static final String IS_LOGGED = "IS_LOGGED";
    public static final int FIRST_PAGE = 0;
    public static final int SECOND_PAGE = 1;
    public static final int THIRD_PAGE = 2;
}
