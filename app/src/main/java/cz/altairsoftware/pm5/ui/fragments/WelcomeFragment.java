package cz.altairsoftware.pm5.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.altairsoftware.pm5.R;
import cz.altairsoftware.pm5.adapters.WelcomeImageAdapter;
import cz.altairsoftware.pm5.ui.activities.InitActivity;

import static cz.altairsoftware.pm5.utils.Constants.SIGN_IN;
import static cz.altairsoftware.pm5.utils.Constants.SIGN_UP;
import static cz.altairsoftware.pm5.utils.Constants.WHERE_TO;

public class WelcomeFragment extends Fragment {

    @BindView(R.id.vpPager)
    ViewPager mPager;
    @BindView(R.id.tabDots)
    TabLayout mIndicatorDots;

    private Unbinder unbinder;
    private WelcomeImageAdapter mPagerAdapter;

    public WelcomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        unbinder = ButterKnife.bind(this, view);
        //Initialize View Pager
        mPagerAdapter = new WelcomeImageAdapter(getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mIndicatorDots.setupWithViewPager(mPager, true);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.signIn)
    public void signIn(){
        startSpecificActivity(InitActivity.class, SIGN_IN);
    }

    @OnClick(R.id.signUp)
    public void signUp(){
        startSpecificActivity(InitActivity.class, SIGN_UP);
    }


    public void startSpecificActivity(Class<?> otherActivityClass, int whereTo) {
        Intent intent = new Intent(getContext(), otherActivityClass);
        intent.putExtra(WHERE_TO, whereTo);
        startActivity(intent);
    }
}
