package cz.altairsoftware.pm5.ui.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.altairsoftware.pm5.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class Pm5Fragment extends ListFragment {

    private Unbinder unbinder;
    BluetoothManager btManager;
    BluetoothAdapter btAdapter;
    BluetoothLeScanner btScanner;
    private final static int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    Boolean btScanning = false;
    int deviceIndex = 0;
    ArrayList<BluetoothDevice> devicesDiscovered = new ArrayList<BluetoothDevice>();
    BluetoothGatt bluetoothGatt;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    public Map<String, String> uuids = new HashMap<String, String>();

    // Stops scanning after 5 seconds.
    private Handler mHandler = new Handler();
    private static final long SCAN_PERIOD = 5000;

    //LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
    ArrayList<String> lvBluetoothDevices =new ArrayList<String>();

    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    ArrayAdapter<String> adapter;

    @BindView(R.id.PeripheralTextView)
    TextView tvStatus;
    @BindView(R.id.ConnectButton)
    Button connectToDevice;
    @BindView(R.id.InputIndex)
    EditText deviceIndexInput;
    @BindView(R.id.DisconnectButton)
    Button disconnectDevice;
    @BindView(R.id.StartScanButton)
    Button startScanningButton;
    @BindView(R.id.StopScanButton)
    Button stopScanningButton;
    @BindView(R.id.btn_scan)
    ImageButton btnScan;
    @BindView(R.id.rl_scan_list)
    RelativeLayout rlScanList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pm5, container, false);
        unbinder = ButterKnife.bind(this, view);

        btnScan.setVisibility(VISIBLE);
        rlScanList.setVisibility(GONE);

        adapter=new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1,
                lvBluetoothDevices);
        setListAdapter(adapter);

        deviceIndexInput.setText("0");
        disconnectDevice.setVisibility(View.INVISIBLE);
        stopScanningButton.setVisibility(View.INVISIBLE);

        btManager = (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
        btScanner = btAdapter.getBluetoothLeScanner();

        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }

        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect peripherals.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
        }
        return view;
    }
    @OnClick (R.id.ConnectButton)
    public void setConnectToDevice(){
        connectToDeviceSelected();
    }

    @OnClick (R.id.DisconnectButton)
    public void setDisconnectDevice(){
        disconnectDeviceSelected();
    }

    @OnClick (R.id.StopScanButton)
    public void setStopScanningButton(){
        stopScanning();
    }

    @OnClick (R.id.StartScanButton)
    public void setStartScanningButtons(){
        startScanning();
    }
    @OnClick (R.id.btn_scan)
    public void setStartScanningButton(){
        startScanning();
    }

    // Device scan callback.
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            if (result.getDevice().getName()!=null) {
                devicesDiscovered.add(result.getDevice());
                lvBluetoothDevices.add("Index: " + deviceIndex + ", Device Name: " + result.getDevice().getName() + " rssi: " + result.getRssi() + "\n");
                adapter.notifyDataSetChanged();
                deviceIndex++;
            }
        }
    };

    // Device connect call back
    private final BluetoothGattCallback btleGattCallback = new BluetoothGattCallback() {

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            // this will get called anytime you perform a read or write characteristic operation
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    tvStatus.setText("device read or wrote to\n");
                }
            });
        }

        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            // this will get called when a device connects or disconnects
            System.out.println(newState);
            switch (newState) {
                case 0:
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            tvStatus.setText("device disconnected\n");
                            adapter.notifyDataSetChanged();
                            connectToDevice.setVisibility(VISIBLE);
                            disconnectDevice.setVisibility(View.INVISIBLE);
                        }
                    });
                    break;
                case 2:
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            tvStatus.setText("device connected\n");
                            connectToDevice.setVisibility(View.INVISIBLE);
                            disconnectDevice.setVisibility(VISIBLE);
                        }
                    });

                    // discover services and characteristics for this device
                    bluetoothGatt.discoverServices();

                    break;
                default:
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            tvStatus.setText("we encounterned an unknown state, uh oh\n");
                        }
                    });
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
            // this will get called after the client initiates a 			BluetoothGatt.discoverServices() call
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    tvStatus.setText("device services have been discovered\n");
                }
            });
            displayGattServices(bluetoothGatt.getServices());
        }

        @Override
        // Result of a characteristic read operation
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                byte[] charValue = characteristic.getValue();
                byte flag = charValue[0];
                Log.i("TAG", "Characteristic: " + flag);
            }
        }
    };

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {

        System.out.println(characteristic.getUuid());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }

    public void startScanning() {
        System.out.println("start scanning");
        btScanning = true;
        deviceIndex = 0;
        devicesDiscovered.clear();

        tvStatus.setText("Started Scanning\n");

        btnScan.setVisibility(GONE);
        rlScanList.setVisibility(VISIBLE);

        stopScanningButton.setVisibility(VISIBLE);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                btScanner.startScan(leScanCallback);
            }
        });

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScanning();
            }
        }, SCAN_PERIOD);
    }

    public void stopScanning() {
        System.out.println("stopping scanning");
        tvStatus.setText("Stopped Scanning\n");
        btScanning = false;
        startScanningButton.setVisibility(VISIBLE);
        stopScanningButton.setVisibility(View.INVISIBLE);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                btScanner.stopScan(leScanCallback);
            }
        });
    }

    public void connectToDeviceSelected() {
        tvStatus.setText("Trying to connect to device at index: " + deviceIndexInput.getText() + "\n");
        int deviceSelected = Integer.parseInt(deviceIndexInput.getText().toString());
        bluetoothGatt = devicesDiscovered.get(deviceSelected).connectGatt(getContext(), false, btleGattCallback);
    }

    public void disconnectDeviceSelected() {
        tvStatus.setText("Disconnecting from device\n");
        bluetoothGatt.disconnect();
    }

    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        lvBluetoothDevices.clear();
        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {


            final String uuid = gattService.getUuid().toString();
            System.out.println("Service discovered: " + uuid);
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
//                    tvStatus.append("Service disovered: "+uuid+"\n");
                    lvBluetoothDevices.add("Service disovered: "+"\n\n" +showType(uuid)+ "\n");
                    adapter.notifyDataSetChanged();
                }
            });
            new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic :
                    gattCharacteristics) {
                final String charUuid = gattCharacteristic.getUuid().toString();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
//                        tvStatus.append("Characteristic discovered for service: "+charUuid+"\n");
                        lvBluetoothDevices.add("Characteristic discovered for service: "+ "\n\n"+ showType(charUuid) + "\n");
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        }
    }

    public String showType(String code){
        String type = code;
        if (code.equals("CE060020-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 PM control primary service";
        }
        else if (code.equals("CE060030-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing primary service";
        }
        else if (code.equals("CE060010-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 device information primary service";
        }
        else if (code.equals("00001801-0000-1000-8000-00805F9B34FB".toLowerCase())){
            type= "GATT primary service";
        }
        else if (code.equals("00001800-0000-1000-8000-00805F9B34FB".toLowerCase())){
            type= "GAP primary service";
        }
        else if (code.equals("00002A00-0000-1000-8000-00805F9B34FB".toLowerCase())){
            type= "GAP device name characteristic";
        }
        else if (code.equals("00002A01-0000-1000-8000-00805F9B34FB".toLowerCase())){
            type= "GAP appearance characteristic";
        }
        else if (code.equals("00002A04-0000-1000-8000-00805F9B34FB".toLowerCase())){
            type= "Peripheral preferred connection parameters characteristic";
        }
        else if (code.equals("00002A05-0000-1000-8000-00805F9B34FB".toLowerCase())){
            type= "Service changed characteristic";
        }
        else if (code.equals("CE060010-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 device information primary service";
        }
        else if (code.equals("CE060022-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 PM transmit characteristic";
        }
        else if (code.equals("CE060021-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 PM receive characteristic";
        }
        else if (code.equals("CE060022-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 PM transmit characteristic";
        }
        else if (code.equals("CE060012-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 serial number string characteristic";
        }
        else if (code.equals("CE060013-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 hardware revision string characteristic";
        }
        else if (code.equals("CE060014-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 firmware revision string characteristic";
        }
        else if (code.equals("CE060015-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 manufacturer name string characteristic";
        }
        else if (code.equals("CE060030-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing primary service";
        }
        else if (code.equals("CE060031-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing general status characteristic";
        }
        else if (code.equals("CE060032-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing additional status 1 characteristic";
        }
        else if (code.equals("CE060033-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing additional status 2 characteristic";
        }
        else if (code.equals("CE060034-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing general status and additional status sample rate characteristic";
        }
        else if (code.equals("CE060035-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing stroke data characteristic";
        }
        else if (code.equals("CE060036-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing additional stroke data characteristic";
        }
        else if (code.equals("CE060037-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing split/interval data characteristic";
        }
        else if (code.equals("CE060038-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing additional split/interval data characteristic";
        }
        else if (code.equals("CE060039-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing end of workout summary data characteristic ";
        }
        else if (code.equals("CE06003A-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing end of workout additional summary data characteristic";
        }
        else if (code.equals("CE06003B-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 rowing heart rate belt information characteristic ";
        }
        else if (code.equals("CE060080-43E5-11E4-916C-0800200C9A66".toLowerCase())){
            type= "C2 multiplexed information characteristic";
        }
        return type;
    }
}
