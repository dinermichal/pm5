package cz.altairsoftware.pm5.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cz.altairsoftware.pm5.R;
import cz.altairsoftware.pm5.ui.fragments.Pm5Fragment;
import cz.altairsoftware.pm5.ui.fragments.SignInFragment;
import cz.altairsoftware.pm5.ui.fragments.WelcomeFragment;

import static cz.altairsoftware.pm5.utils.Constants.IS_LOGGED;
import static cz.altairsoftware.pm5.utils.Constants.SIGN_IN;
import static cz.altairsoftware.pm5.utils.Constants.SIGN_UP;
import static cz.altairsoftware.pm5.utils.Constants.WHERE_TO;

public class InitActivity extends AppCompatActivity {


    private int whereTo;
    private Unbinder unbinder;
    private boolean isLogged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container);
        unbinder = ButterKnife.bind(this);

        Bundle getWhereTo = getIntent().getExtras();
        if (getWhereTo != null){
            whereTo = getWhereTo.getInt(WHERE_TO);
            isLogged = getWhereTo.getBoolean(IS_LOGGED, false);
        }

        if (savedInstanceState == null) {
            // Checks if user is logged.
            if (!isLogged) {
                if (whereTo == SIGN_IN) {
                    Fragment fragment = new SignInFragment();
                    openFragment(fragment);
                }
                if (whereTo == SIGN_UP) {
//                    Fragment fragment = new SignUpFragment();
//                    openFragment(fragment);


                    Fragment fragment = new Pm5Fragment();
                    openFragment(fragment);
                }
                else {
                    Fragment fragment = new WelcomeFragment();
                    openFragment(fragment);
                }
            }
            // User is logged in.
            else{
                Fragment fragment = new Pm5Fragment();
                openFragment(fragment);
            }
        }
    }

    public void openFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
